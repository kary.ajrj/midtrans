Feature: Purchase a pillow

  Scenario Outline: Purchase pillow successfully by completing the payment
    Given I am on the pillow store
    When I add a pillow to the cart and checkout
    Then an order summary is displayed to me
    When I continue to payments using Credit card option
    And I submit the <Card number>, <Expiry> and <CVV> details
    And enter the <OTP> for 3DS verification of the credit card
    Then I see the order success confirmation
    Examples:
      | Card number         | Expiry | CVV | OTP    |
      | 4811 1111 1111 1114 | 1224   | 123 | 112233 |

  # This scenario is solely to check the end to end flow for a pillow purchase.
  # Hence, it has minimal checks for the intermediate pages.
  # Any other tests like for Customer details, list of payment options, billing details can be added separately,
  # or can be covered in the lower level tests.