Feature: Payment failure scenarios

  Scenario Outline: Transaction failure when purchasing a pillow due to incorrect card details
    Given I am on the pillow store
    When I add a pillow to the cart and checkout
    Then an order summary is displayed to me
    When I continue to payments using Credit card option
    And I submit the <Card number>, <Expiry> and <CVV> details
    And enter the <OTP> for 3DS verification of the credit card
    Then I see the transaction failure
# NOTE : Changed the expiry, since the one given in the PDF does not satisfy the failure criterion.
    Examples:
      | Card number         | Expiry | CVV | OTP    |
      | 4811 1111 1111 1114 | 1224   | 123 | 112243 |