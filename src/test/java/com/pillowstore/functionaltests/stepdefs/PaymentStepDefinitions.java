package com.pillowstore.functionaltests.stepdefs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PaymentStepDefinitions extends BaseStepDefinition {

    @When("^I submit the ([^\"]*), ([^\"]*) and ([^\"]*) details$")
    public void iEnterTheDetailsOfTheCard(String cardNumber, String expiry, String cvv) {

        secure3DSPage = ccPaymentPage.enterCreditCardDetails(cardNumber, expiry, cvv)
                                        .submit();
    }

    @When("^enter the ([^\"]*) for 3DS verification of the credit card$")
    public void iEnterOtp(String otp) {

        paymentConfirmationPanel = secure3DSPage.enterOTP(otp)
                                                    .submit();
    }

    @Then("^I see the order success confirmation$")
    public void iSeeTheOrderSuccessConfirmation() {
        paymentConfirmationPanel.verifyTransactionSuccess();
    }

    @Then("^I see the transaction failure$")
    public void failedTransaction() {
        paymentConfirmationPanel.verifyTransactionFailure().verifyRetryOptionPresent();
    }
}