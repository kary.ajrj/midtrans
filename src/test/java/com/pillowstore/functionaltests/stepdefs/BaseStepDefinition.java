package com.pillowstore.functionaltests.stepdefs;

import com.pillowstore.functionaltests.pages.*;

public class BaseStepDefinition {
    static Homepage page;
    static ShoppingCart shoppingCart;
    static CreditCardPaymentPage ccPaymentPage;
    static Secure3DSPage secure3DSPage;
    static PaymentConfirmationPanel paymentConfirmationPanel;

}