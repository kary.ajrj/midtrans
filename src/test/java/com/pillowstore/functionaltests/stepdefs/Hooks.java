package com.pillowstore.functionaltests.stepdefs;

import com.pillowstore.functionaltests.utilities.ScreenshotUtil;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;

import static com.pillowstore.functionaltests.utilities.DriverUtils.driver;

public class Hooks extends BaseStepDefinition {

    @After
    public void teardown(Scenario scenario) {

        if (scenario.isFailed()) {
            ScreenshotUtil.takeScreenshot(driver(), scenario);
        }
        page.closeBrowserSession();
    }
}