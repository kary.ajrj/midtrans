package com.pillowstore.functionaltests.stepdefs;

import com.pillowstore.functionaltests.pages.Homepage;
import com.pillowstore.functionaltests.utilities.DriverUtils;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PillowStoreStepDefinitions extends BaseStepDefinition {

    @Given("I am on the pillow store")
    public void iAmOnThePillowStore() {
        page = new Homepage(DriverUtils.driver());
    }

    @When("I add a pillow to the cart and checkout")
    public void iAddAPillowToTheCartAndCheckout() {
        shoppingCart = page.buyPillow();
        shoppingCart.checkout();
    }

    @When("I continue to payments using Credit card option")
    public void iContinueToCreditCardPaymentPage() {
        ccPaymentPage = shoppingCart.continueToPayments().selectPaymentOption("credit-card");
    }

    @Then("^an order summary is displayed to me")
    public void anOrderSummaryIsDisplayed() {
        shoppingCart.verifyOrderSummary();
    }
}