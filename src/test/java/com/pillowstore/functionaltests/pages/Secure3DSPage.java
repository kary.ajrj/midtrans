package com.pillowstore.functionaltests.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Secure3DSPage extends Page {
    public Secure3DSPage(WebDriver driver) {
        super(driver);
        driver.switchTo().frame(0);
    }

    public Secure3DSPage enterOTP(String otp) {
        waitFor(By.id("PaRes"));
        fetchElement(By.id("PaRes")).sendKeys(otp);
        return this;
    }

    public PaymentConfirmationPanel submit() {
        fetchElement(By.name("ok")).click();
        return new PaymentConfirmationPanel(driver);
    }
}