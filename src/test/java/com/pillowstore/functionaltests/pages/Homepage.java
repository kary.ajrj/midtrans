package com.pillowstore.functionaltests.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Homepage extends Page{

    public Homepage(WebDriver driver) {
        super(driver);
        driver.get("https://demo.midtrans.com");
    }

    public ShoppingCart buyPillow() {
        fetchElement(By.linkText("BUY NOW")).click();
        return new ShoppingCart(driver);
    }
}