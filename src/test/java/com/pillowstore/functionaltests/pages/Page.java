package com.pillowstore.functionaltests.pages;

import com.pillowstore.functionaltests.utilities.DriverUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {
    WebDriver driver;
    WebDriverWait wait;

    public Page(WebDriver driver) {
        this.driver = driver;
        wait = DriverUtils.getWebDriverWait(driver);
    }

    void waitFor(By locator) {
        fetchElement(locator);
    }

    WebElement fetchElement(By locator) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void closeBrowserSession() {
        driver.quit();
    }

}