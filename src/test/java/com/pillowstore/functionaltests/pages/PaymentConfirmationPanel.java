package com.pillowstore.functionaltests.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

public class PaymentConfirmationPanel extends Page{
    public PaymentConfirmationPanel(WebDriver driver) {
        super(driver);
    }

    public PaymentConfirmationPanel verifyTransactionSuccess() {
        waitUntilHomePageIsDisplayed();

        String status = fetchElement(By.cssSelector(".trans-status")).getText();
        assertThat(status, containsString("Thank you for your purchase."));
        return this;
    }

    private void waitUntilHomePageIsDisplayed() {
        //Payment has nested iframes.
        // On Successful payment they are exited and homepage is displayed to user
        driver.switchTo().parentFrame();
        driver.switchTo().parentFrame();

    }

    public PaymentConfirmationPanel verifyTransactionFailure() {

        driver.switchTo().parentFrame();

        String result = fetchElement(By.cssSelector("#app .text-failed")).getText();
        assertThat(result, containsString("Transaction failed"));
        return this;
    }

    public PaymentConfirmationPanel verifyRetryOptionPresent() {
        WebElement retryButton = fetchElement(By.xpath("//a//span[text()='Use Another Payment Options']"));
        assertTrue(retryButton.isEnabled());
        return this;
    }
}