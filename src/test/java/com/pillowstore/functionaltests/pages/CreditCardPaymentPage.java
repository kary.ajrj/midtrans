package com.pillowstore.functionaltests.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreditCardPaymentPage extends Page {

    public CreditCardPaymentPage(WebDriver driver) {
        super(driver);
    }

    public CreditCardPaymentPage enterCreditCardDetails(String cardNumber, String expiry, String cvv) {

        fetchElement(By.name("cardnumber")).sendKeys(cardNumber);
        fetchElement(By.cssSelector("input[placeholder='MM / YY']")).sendKeys(expiry);
        fetchElement(By.xpath("//label[text()='CVV']/../input")).sendKeys(cvv);

        return this;
    }

    public Secure3DSPage submit() {
        fetchElement(By.xpath("//span[text()='Pay Now']/../..")).click();
        return new Secure3DSPage(driver);
    }
}