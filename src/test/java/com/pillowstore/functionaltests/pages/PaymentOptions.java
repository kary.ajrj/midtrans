package com.pillowstore.functionaltests.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaymentOptions extends Page {

    public PaymentOptions(WebDriver driver) {
        super(driver);
    }

    public CreditCardPaymentPage selectPaymentOption(String paymentOption) {
        fetchElement(By.cssSelector("#payment-list a[href='#/" + paymentOption + "']")).click();
        return new CreditCardPaymentPage(driver);
    }
}