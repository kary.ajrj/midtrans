package com.pillowstore.functionaltests.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ShoppingCart extends Page {

    public ShoppingCart(WebDriver driver) {
        super(driver);
    }

    public ShoppingCart checkout() {
        waitFor(By.className("cart-checkout"));
        fetchElement(By.className("cart-checkout")).click();
        return this;
    }

    public ShoppingCart verifyOrderSummary() {

        driver.switchTo().frame(0);
        WebElement pageTitle = fetchElement(By.className("text-page-title-content"));

        String frameTitle = pageTitle.getText();
        assertThat(frameTitle, is("Order Summary"));
        return this;
    }

    public PaymentOptions continueToPayments() {
        WebElement continueToPaymentOptionsButton = fetchElement(By.xpath(".//a[@href='#/select-payment']"));
        continueToPaymentOptionsButton.click();

        return new PaymentOptions(driver);
    }
}