package com.pillowstore.functionaltests.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class DriverUtils {

    private static WebDriver webDriver;
    static String browserType;

    public static WebDriver driver() {

        if (webDriver == null) {
            getANewInstanceOfDriver();
        } else if ("chrome".equalsIgnoreCase(browserType)
                && ((ChromeDriver) webDriver).getSessionId() == null) {
            chromeDriver();
        } else if ("firefox".equalsIgnoreCase(browserType)
                && ((FirefoxDriver) webDriver).getSessionId() == null) {
            firefoxDriver();
        }
        return webDriver;
    }

    private static void getANewInstanceOfDriver() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        System.setProperty("webdriver.chrome.driver","chromedriver");

        browserType = System.getProperty("browserName");

        if ("CHROME".equalsIgnoreCase(browserType)) {
            chromeDriver();
        } else if ("FIREFOX".equalsIgnoreCase(browserType)) {
            firefoxDriver();
        } else {
            browserType = "FIREFOX";
            firefoxDriver();
        }
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }

    private static void firefoxDriver() {
        webDriver = new FirefoxDriver();
    }

    private static void chromeDriver() {
        webDriver = new ChromeDriver();
    }

    public static WebDriverWait getWebDriverWait(WebDriver driver) {
        return new WebDriverWait(driver, Duration.ofSeconds(50));
    }
}