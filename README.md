## Midtrans pillow store test framework.

_This repo contains a UI test framework along with functional tests for the Midtrans Pillow store web application._

Framework is created using **_Cucumber + Selenium Webdriver + Java_**.

This is a gradle project. There is no need of installing gradle separately. The gradle wrapper will take care of getting the correct version of gradle and running the commands. 

##To run the tests:

`$ ./gradlew clean cucumber -DbrowserName={browserName}`

If no browserName argument is supplied, the tests would be executed on Firefox by default.
Latest drivers (as of today) have been used.

_**Please note :**_ Due to issues with the latest Chrome version and the driver, the tests currently do not run on Chrome with an exception on the lines of:
`org.openqa.selenium.ElementNotInteractableException: element not interactable: element has zero size`. None of the plausible reasons for this error do not seem to be applicable.

HTML reports are generated with every run and stored in the **build/cucumber** folder. 

## Approach for creating the test framework:

* I started with getting the gradle project ready with the necessary dependencies. 

* Then added a basic test to check if the framework was intact and test were executed with the Firefox browser.

* Once that was done, I focused on adding the tests for successful purchase, the simplest way I could. 
Here I did not write any page objects, just got the test running with proper assertions.

* Next step was to make the framework support chrome as well as firefox browsers. 
A system property with desired browserName needs to be passed to the gradle command as mentioned above. 

    `$ ./gradlew clean cucumber -DbrowserName=FIREFOX`
 
* Reporting was added after this and also the code to capture screenshots on failure.
Please refer to _ScreenshotUtility.java_ & _Hooks.java_ for the code. 

* Finally, when all the above was working seamlessly, I went ahead an invested time in making the tests readable by adding Page objects.
I've also tried to follow the Single responsibility principle for better segregation of the test code.

## Things that can be improved:
* A better reporting plugin can be integrated. Currently, the framework are using Cucumber's inbuilt reporter to get basic reports.
The following plugin is a much better option and adds charts/graphs along with the feature reports 
 `https://github.com/damianszczepanik/cucumber-reporting`
* There is scope for improvement in the page objects, segregating some frame switching code.
* The language in the feature file can be improved. Generally, talking to people closer to the business (like a BA) helps over here.